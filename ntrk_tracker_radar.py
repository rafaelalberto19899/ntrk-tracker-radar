#!/usr/bin/env python3
#Title   : NoTrack Tracker Radar Processor
#Description : This script will process DuckDuckGo Tracker Radar list into five seperate files for use in a DNS server like NoTrack and PiHole.
#              Lists are categorised as Confirmed, High, Medium, Low, and Unknown based on the certainty of a domain being a tracker
#Author  : QuidsUp
#Date    : 2020-06-28
#Version : 0.2
#Usage   : git clone https://github.com/duckduckgo/tracker-radar.git
#          python3 ntrk_tracker_radar.py

#Standard Imports
import datetime
import json
import os
import re

class Ntrk_TrackerRadar():
    def __init__(self):
        #Categories are taken from [https://github.com/duckduckgo/tracker-radar/blob/master/docs/CATEGORIES.md]
        self.excluded_categories = {             #Categories to exclude which have a purpose other than tracking
            'CDN',
            'Embedded Content',
            'Federated Login',
            'Non-Tracking',
            'Online Payment',
            'Social Network',
            'SSO',
        }

        self.advertising_categories = {          #Known Advertising categories
            'Advertising',
        }

        self.tracker_categories = {              #Known Tracker categories
            'Action Pixels',
            'Ad Motivated Tracking',
            'Analytics',
            'Third-Party Analytics Marketing',
        }

        self.malware_categories = {              #Known Malware categories
            'Malware',
        }

        self.ignore_domains = {                  #Domains to exclude from complete blocking as they will cause noticeable breakage
            'amazon-adsystem.com',               #Essential for Amazon Android / iPhone app
            'azureedge.net',                     #Azure cloud services, hosts many legitimate domains
            'cloudfront.net',                    #CDN
            'cloudflare.net',                    #CDN
            'hcaptcha.com',                      #Bot Protection, used on login screens
            'walmart.com',                       #US Supermarket, also used by asda.co.uk
            'yahoo.co.jp',
        }

        self.__processed_domains = set()         #Prevent duplication

        self.__certain_domains = []              #Confirmed Tracker / Advertising / Malware Domains
        self.__high_domains = []
        self.__med_domains = []
        self.__low_domains = []
        self.__unknown_domains = []

        #Regex to extract domain.co.uk from subdomain.domain.co.uk
        self.__REGEX_DOMAIN = re.compile(r'([\w\-_]{1,63}\.(?:co\.|com\.|org\.|edu\.|gov\.)?[\w\-_]{1,63}$)')

        #Regex to identify known advertising domains by beginning subdomain
        #ads / adv, advertising, tag. Followed by optional 2-3 char domain
        self.__REGEX_ADVERTISING = re.compile(r'^(ad(s|v)|advertising|tag)(\.\w{2,3})?$')

        #Regex to identify known tracking subdomains
        #analytics, beacon / beacons, click, heatmap / heatmaps, insights, log / logger / logging, metrics / pixel, pixl / px / pix / pxl, stat / stats
        #telemetry, track / trck / tracker / tracking, trk (plus optional digit)
        #Optional domain .mtrcs, srvcs
        #Followed by optional 2-3 char domain
        self.__REGEX_TRACKER = re.compile(r'^(analytics|beacons?|click|heatmaps?|insights|log(ger|ing)?|metrics|pixel|pi?xl?|stats?|telemetry|tra?ck(er|ing)?|trk\d?)(\.mtrcs|srvcs)?(\.\w{2,3})?$')


    def __add_certain(self, domain, owner, category):
        """
        Add a domain to __certain_domains as long as it hasn't already been added
        """
        if domain in self.__processed_domains:
            return

        self.__processed_domains.add(domain)
        self.__certain_domains.append(tuple([domain, owner, category]))


    def __add_high(self, domain, owner, category):
        """
        Add a domain to __high_domains as long as it hasn't already been added
        """
        if domain in self.__processed_domains:
            return

        self.__processed_domains.add(domain)
        self.__high_domains.append(tuple([domain, owner, category]))


    def __add_med(self, domain, owner, category):
        """
        Add a domain to __med_domains as long as it hasn't already been added
        """
        if domain in self.__processed_domains:
            return

        self.__processed_domains.add(domain)
        self.__med_domains.append(tuple([domain, owner, category]))


    def __add_low(self, domain, owner, category):
        """
        Add a domain to __low_domains as long as it hasn't already been added
        """
        if domain in self.__processed_domains:
            return

        self.__processed_domains.add(domain)
        self.__low_domains.append(tuple([domain, owner, category]))


    def __add_unknown(self, domain, owner, category):
        """
        Add a domain to __unknown_domains as long as it hasn't already been added
        """
        if domain in self.__processed_domains:
            return

        self.__processed_domains.add(domain)
        self.__unknown_domains.append(tuple([domain, owner, category]))

    def __is_advertising(self, categories):
        """
        Checks if categories include Advertising categories and not Excluded categories
        """
        if categories and categories & self.advertising_categories:
            if categories and categories & self.excluded_categories:
                return False
            else:
                return True

        return False


    def __is_tracker(self, categories):
        """
        Checks if categories include Tracker categories and not Excluded categories
        """
        if categories and categories & self.tracker_categories:
            if categories and categories & self.excluded_categories:
                return False
            else:
                return True

        return False


    def __is_malware(self, categories):
        """
        Checks if categories include Tracker categories and not Excluded categories
        """
        if categories and categories & self.malware_categories:
            if categories and categories & self.excluded_categories:
                return False
            else:
                return True

        return False

    def __is_ignore(self, domain, categories):
        """
        Checks if domain is in Ignore domains
        Checks Categories does not include Excluded categories
        """
        if domain in self.ignore_domains:
            return True

        if categories and categories & self.excluded_categories:
            return True
        else:
            return False

        return False


    def __is_advertising_subdomain(self, subdomain):
        """
        Checks if subdomain matches __REGEX_ADVERTISING
        """
        if self.__REGEX_ADVERTISING.match(subdomain) is None:
            return False
        return True


    def __is_tracker_subdomain(self, subdomain):
        """
        Checks if subdomain matches __REGEX_TRACKER
        """
        if self.__REGEX_TRACKER.match(subdomain) is None:
            return False
        return True

    def __beautify_domain(self, domain):
        """
        Extract domain from subdomain
        Return UC-First domain

        Parameters:
            Domain (str)
        Returns:
            Beautified domain name
        """
        topdomain = self.__REGEX_DOMAIN.search(domain)

        if topdomain == None:                              #This shouldn't happen
            return domain.title()

        return topdomain.group(1).title()                  #TopDomain in Title case


    def __process_cnames(self, cnames, itemtype):
        """
        Process cnames
        Add all identified original domains to certain blocklist
        """
        origdomain = ''                                    #Original Domain

        if not cnames:
            return False

        for cname in cnames:
            origdomain = cname['original']

            if (self.__is_tracker_subdomain(origdomain)):  #Check if subdomain matches a tracker pattern
                self.__add_certain(origdomain, self.__beautify_domain(origdomain), 'Tracker')
            else:                                          #Otherwise use the default from DDG
                self.__add_certain(origdomain, self.__beautify_domain(origdomain), itemtype)


    def __process_subdomains(self, domain, subdomains):
        """
        Process subdomains
        Check if any subdomains match advertising or tracker regex pattern

        Parameters:
            domain (str): Domain Name
            subdomain (list): List of subdomains
        Returns:
            True: At least 1 tracker found in subdomain list
            False: Nothing useful found
        """
        containstracker = False                            #Return value to say a tracker subdomain has been found

        if len(subdomains) == 0:                           #Anything in the subdomain list?
            return False

        for subdomain in subdomains:
            if self.__is_tracker_subdomain(subdomain):
                self.__add_certain(f'{subdomain}.{domain}', domain.title(), 'Tracker')
                containstracker = True
            elif self.__is_advertising_subdomain(subdomain):
                self.__add_certain(f'{subdomain}.{domain}', domain.title(), 'Advertising')
                containstracker = True

        return containstracker


    def __process_lists(self):
        """
        Process Lists
        Sort by domain name a-z
        Display Results
        Send list to be saved to disk
        """
        self.__certain_domains.sort(key=lambda x: x[0])    #Sort by domain
        self.__high_domains.sort(key=lambda x: x[0])       #Sort by domain
        self.__med_domains.sort(key=lambda x: x[0])        #Sort by domain
        self.__low_domains.sort(key=lambda x: x[0])        #Sort by domain
        self.__unknown_domains.sort(key=lambda x: x[0])    #Sort by domain

        print('Results:')
        print(f'Confirmed Trackers: {len(self.__certain_domains)}')
        print(f'High Probability  : {len(self.__high_domains)}')
        print(f'Medium Probability: {len(self.__med_domains)}')
        print(f'Low Probability   : {len(self.__low_domains)}')
        print(f'Unknown Status    : {len(self.__unknown_domains)}')

        self.__save_list(self.__certain_domains, 'ddg_tracker_radar_confirmed.txt', 'Confirmed')
        self.__save_list(self.__high_domains, 'ddg_tracker_radar_high.txt', 'High')
        self.__save_list(self.__med_domains, 'ddg_tracker_radar_med.txt', 'Medium')
        self.__save_list(self.__low_domains, 'ddg_tracker_radar_low.txt', 'Low')
        self.__save_list(self.__unknown_domains, 'ddg_tracker_radar_unknown.txt', 'Unknown')

        self.__save_hosts(self.__certain_domains, 'ddg_tracker_radar_confirmed.hosts', 'Confirmed')
        self.__save_hosts(self.__high_domains, 'ddg_tracker_radar_high.hosts', 'High')
        self.__save_hosts(self.__med_domains, 'ddg_tracker_radar_med.hosts', 'Medium')
        self.__save_hosts(self.__low_domains, 'ddg_tracker_radar_low.hosts', 'Low')
        self.__save_hosts(self.__unknown_domains, 'ddg_tracker_radar_unknown.hosts', 'Unknown')


    def __save_hosts(self, domains, filename, listtype):
        """
        Save a blocklist as a hosts file format

        Parameters:
            domains (list): list of data to save to file
            filename (str): File to save
            listtype (str): For file comments
        Returns:
            True on success
            False on error
        """
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        try:
            f = open(filename, 'w')                        #Open file for ascii writing
        except IOError as e:
            print(f'Error writing to {filename}')
            print(e)
            return False
        except OSError as e:
            print(f'Error writing to {filename}')
            print(e)
            return False
        else:
            f.write(f'# DuckDuckGo Tracker Radar {listtype} Domains processed by Ntrk_TrackerRadar\n')
            f.write(f'# Original Data: https://github.com/duckduckgo/tracker-radar\n')
            f.write(f'# Project Page: https://gitlab.com/quidsup/ntrk-tracker-radar\n')
            f.write(f'# Blocklist Generated {timestamp}\n')

            for line in domains:
                f.write(f'0.0.0.0 {line[0]}\n')
            f.close()
        return True


    def __save_list(self, domains, filename, listtype):
        """
        Save a blocklist as a plainlist format

        Parameters:
            domains (list): list of data to save to file
            filename (str): File to save
            listtype (str): For file comments
        Returns:
            True on success
            False on error
        """
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        try:
            f = open(filename, 'w')                        #Open file for ascii writing
        except IOError as e:
            print(f'Error writing to {filename}')
            print(e)
            return False
        except OSError as e:
            print(f'Error writing to {filename}')
            print(e)
            return False
        else:
            f.write(f'# DuckDuckGo Tracker Radar {listtype} Domains processed by Ntrk_TrackerRadar\n')
            f.write(f'# Original Data: https://github.com/duckduckgo/tracker-radar\n')
            f.write(f'# Project Page: https://gitlab.com/quidsup/ntrk-tracker-radar\n')
            f.write(f'# Blocklist Generated {timestamp}\n')

            for line in domains:
                f.write(f'{line[0]} #{line[1]} - {line[2]}\n')
            f.close()
        return True


    def process_file(self, filename):
        """
        Read a JSON file and process the domain data

        Parameters:
            filename (str): File to read
        Returns:
            None
        """
        cnames = []
        domain = ''
        owner = ''                                         #Owner information or domain name

        data = json.load(open(filename, "r"))              #Read JSON data from file

        domain = data['domain']

        categories = set(data['categories'])           #Convert categories list to a set
        subdomains = data['subdomains']

        if 'cnames' in data:                           #Some files lack a cname section
            cnames = data['cnames']
        else:
            cnames = []

        if not data['owner']:                          #Some files lack anything in owner
            owner = domain                             #Default to domain instead
        else:
            owner = data['owner']['displayName']

        #1. Add known tracker categorisation to certain list
        if self.__is_tracker(categories) and domain not in self.ignore_domains:
            self.__add_certain(domain, owner, 'Tracker')
            self.__process_cnames(cnames, 'Tracker')
            return

        #2. Add known advertising categorisation to certain list
        if self.__is_advertising(categories) and domain not in self.ignore_domains:
            self.__add_certain(domain, owner, 'Advertising')
            self.__process_cnames(cnames, 'Advertising')
            return

        #3. Add known malware categorisation to certain list
        if self.__is_malware(categories):
            self.__add_certain(domain, owner, 'Malware')
            return

        #4. High certainty fingerprinting to high list
        if data['fingerprinting'] == 3:
            if not self.__is_ignore(domain, categories):
                self.__add_high(domain, owner, 'Tracker High Certainty')
                return

        #5. Any tracking / advertising subdomains to certain list
        #   Disregard ignore domains and ignore categories for subdomains
        if self.__process_subdomains(domain, subdomains):
            return

        #6. Medium certainty to med list
        if data['fingerprinting'] == 2:
            if not self.__is_ignore(domain, categories):
                self.__add_med(domain, owner, 'Tracker Medium Certainty')
                return

        #7. Low certainty to low list
        if data['fingerprinting'] == 1:
            if not self.__is_ignore(domain, categories):
                self.__add_low(domain, owner, 'Tracker Low Certainty')
                return

        #At this point no other match has taken place.
        self.__add_unknown(domain, owner, 'Unknown Status')
        return True


    def read_dir(self, domainsdir):
        """
        Process Files

        Parameters:
            domainsdir (str): Location of DDG Tracker Radar domains folder
        """
        file_count = 0                                     #Number of files processed

        print(f'Reading json files from {domainsdir}')
        file_list = os.scandir(domainsdir)

        for root,dirs,files in os.walk(domainsdir):
            for f in files:
                filename = os.path.join(root, f)

                #Ignore anything other than a .json file
                if not filename.endswith('.json'):
                    continue

                file_count += 1
                self.process_file(filename)



        print(f'Processed {file_count} files')
        print()

        self.__process_lists()                             #Finally sort the list and then save to disk


def main():
    print('NoTrack Tracker Radar Blocklist Processor')
    print()
    trackerradar = Ntrk_TrackerRadar()
    trackerradar.read_dir('../tracker-radar/domains/')


if __name__ == "__main__":
    main()

